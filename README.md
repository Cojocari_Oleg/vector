//Создайте класс, который описывает вектор (в трёхмерном пространстве).
import java.util.Scanner;
import static java.lang.StrictMath.sqrt;
public class Vector {
    public static void main(String[] args) {
        RezolvVector vector = new RezolvVector();
        Scanner s = new Scanner(System.in);
        //inser later X
        vector.mesage("X");
        if(s.hasNextDouble())  vector.setX(s.nextDouble());
            else System.out.println("it is not number");
        //inser later Y
        vector.mesage("Y");
        if(s.hasNextDouble())  vector.setY(s.nextDouble());
            else System.out.println("it is not number");
        //inser later Z
        vector.mesage("Z");
        if(s.hasNextDouble())  vector.setZ(s.nextDouble());
        else System.out.println("it is not number");
        System.out.println(vector.getX() + "  "+ vector.getY()+"   "+ vector.getZ());
        new RezolvVector(vector.getX(), vector.getY(), vector.getZ());//resultat
    }
}
class RezolvVector{
   double x, z, y;
    public void setX(double x) {
        if(x > 0)
            this.x = x;
        else
            System.out.println("vector x is negativ number, the vector mast by poyitive");
    }
    public double getX() {return x;}
    public void setZ(double z) {
        if (z > 0)
            this.z = z;
        else
            System.out.println("vector z is negativ number, the vector mast by poyitive");
    }
    public double getZ() { return z;}
    public void setY(double y) {
        if (y > 0)
            this.y = y;
        else
            System.out.println("vector y is negativ number, the vector mast by poyitive");
    }
    public double getY() { return y;}
    public RezolvVector(double x, double y, double z){
       this.x = getX();
       this.y = getY();
       this.z = getZ();
       System.out.println("length of the vector is = " + sqrt(x * x + y * y + z * z));// vector length
       System.out.println("mean of arithmetic is = " + ((x+y+z)/3 )); //arithmetic mean
        maxMin(x,y,z); // max and min
    }
    void maxMin(double x, double y, double z ){
        this.x = getX();
        this.y = getY();
        this.z = getZ();
        if ((x>z) & (x>y)) System.out.println("maximal is vector is = "+ x);
        if ((z>x) & (z>y)) System.out.println("maximal is vector is = "+ z);
        if ((y>x) & (y>z)) System.out.println("maximal is vector is = "+ y);
        if ((x<z) & (x<y)) System.out.println("minimal is vector is = "+ x);
        if ((z<x) & (z<y)) System.out.println("minimal is vector is = "+ z);
        if ((y<x) & (y<z)) System.out.println("minimal is vector is = "+ y);
   }
    void mesage(String x){
       System.out.println("inser please value of the "+ x + " side");
    }
}
